extern crate fractal_flames;
extern crate rand;
extern crate ron;
extern crate rayon;

#[macro_use]
extern crate serde;

extern crate png;
use png::HasParameters;
use std::io::BufWriter;
use std::error::Error;

use rayon::prelude::*;
use std::io::prelude::*;
use std::fs::File;
use ron::de::from_reader;
use std::path::Path;
use ron::ser::{to_string_pretty, PrettyConfig};
use fractal_flames::*;
use rand::prelude::*;

use std::time::Instant;

fn get_or_create_scene(path: &Path, palettes_filepath: &Path) -> Scene {
    match get_scene_from_file(path) {
        Ok(scene) => scene,
        Err(e) => {
            println!("WARNING -- Could not read scene from file {}: {}", path.to_string_lossy(), e);

            let scene = create_scene(palettes_filepath);

            let pretty = PrettyConfig {
                    depth_limit: 4,
                    separate_tuple_members: true,
                    ..PrettyConfig::default()
            };

            let s = to_string_pretty(&scene, pretty).expect("Serialization failed");

            let mut f = File::create(&path).expect("Failed to create file for functions");
            f.write_all(s.as_bytes()).expect("Failed to write functions to file");

            scene
        }
    }
}

fn main() {
    let scene_filepath = Path::new("/work/jason/projects/fractal-flames-distributed/scene.ron");
    // let palettes_filepath = Path::new("/work/jason/projects/fractal-flames-distributed/flam3-palettes.ron");

    let scene: Scene = create_scene(palettes_filepath);

    let width = 1920 as u32;
    let height = 1080 as u32;
    let quality = 900 as f32;
    let ssaa_factor = 3 as u32;

    let num_threads = 24;

    let relative_quality = quality as f32 / num_threads as f32;

    // let seq_start = Instant::now();
    // let t = run_scene_rng(scene.clone(), width, height, quality);
    // println!("sequential: {}s", seq_start.elapsed().as_secs());

    let ssaa_width = width * ssaa_factor;
    let ssaa_height = height * ssaa_factor;

    let par_start = Instant::now();
    let histogram: Histogram = (0..num_threads).into_par_iter().map(|_| {
        run_scene_rng(scene.clone(), ssaa_width, ssaa_height, relative_quality)
    }).reduce(||Histogram::new(ssaa_width, ssaa_height), |a, b| a + b).do_ssaa(ssaa_factor);
    println!("parallel: {}s", par_start.elapsed().as_secs());


    let kernel_size = 5 as u32;
    let sigma = 1.0;
    let epanechnikov_kernel = get_epanechnikov_kernel(kernel_size, sigma);
    let frequencies = histogram.buckets.iter().map(|bucket| bucket.frequency as f32).collect();

    let mut density = convolve2d(frequencies, width, height, epanechnikov_kernel, kernel_size);

    for x in &mut density {
        *x = f32::log(*x + 0.001, 10.0);
    }

    let min_density = density.iter().cloned().fold(0./0., f32::min);
    let max_density = density.iter().cloned().fold(0./0., f32::max);

    for x in &mut density {
        *x = (*x - min_density)/(max_density - min_density);
    }

    print_density(&density, width, height, "density.png");

    let colors = histogram.scale_colors();

    let max_blur_radius = 5 as usize;
    let new_colors = blur(colors, density, width, height, max_blur_radius);

    let data: Vec<u8> = new_colors
            .iter()
            .flat_map(|color| color.as_u8_slice().to_vec())
            .collect();

    let path = "histo.png";

    let file = match File::create(&path) {
        Err(e) => panic!("couldn't open file {}: {}", path, e.description()),
        Ok(file) => file,
    };

    let ref mut w = BufWriter::new(file);

    let mut encoder = png::Encoder::new(w, width as u32, height as u32);
    encoder.set(png::ColorType::RGB).set(png::BitDepth::Eight);
    let mut writer = encoder.write_header().unwrap();

    writer.write_image_data(&data).unwrap();

    // histogram.write_file("histo.png");
}
