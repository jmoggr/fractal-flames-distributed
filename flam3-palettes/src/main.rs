extern crate serde_derive;
extern crate serde_xml_rs;
extern crate ron;

#[macro_use]
extern crate serde;

use std::fs::File;
use std::io::prelude::*;
use std::path::Path;
use std::str;

#[derive(Deserialize, Debug)]
struct XMLPalettes {
    #[serde(rename = "$value")]
    palettes: Vec<XMLPalette>,
}

#[derive(Deserialize, Debug)]
struct XMLPalette {
    number: String,
    name: String,
    data: String,
}

#[derive(Serialize, Deserialize, Debug)]
struct Palette {
    number: u32,
    name: String,
    colors: Vec<PaletteColor>,
}

#[derive(Serialize, Deserialize, Debug)]
struct PaletteColor {
    r: u8,
    g: u8,
    b: u8,
}

impl PaletteColor {
    pub fn new_from_vec(rgb: Vec<u8>) -> PaletteColor {
        PaletteColor {
            r: rgb[0],
            g: rgb[1],
            b: rgb[2],
        }
    }
}

fn main() {
    let f = File::open("/work/jason/projects/fractal-flames-distributed/flam3-palettes/flam3-palettes.xml").expect("could not open file");

    let xml_palettes = serde_xml_rs::deserialize::<_, XMLPalettes>(f)
        .expect("could not parse")
        .palettes;

    let palettes = xml_palettes
        .iter()
        .map(|xml_palette| {
            let data = xml_palette
                .data
                .chars()
                .filter(|c| *c != '\n')
                .collect::<String>();

            let color_strings = data
                .as_bytes()
                .chunks(8)
                .map(str::from_utf8)
                .collect::<Result<Vec<&str>, _>>()
                .unwrap();

            let palette_colors = color_strings
                .iter()
                .map(|color_string| {
                    let color_bytes = color_string
                        .as_bytes()
                        .chunks(2)
                        .map(str::from_utf8)
                        .collect::<Result<Vec<&str>, _>>()
                        .unwrap();
                    let rgb: Vec<u8> = color_bytes
                        .iter()
                        .skip(1)
                        .map(|t| u8::from_str_radix(t, 16).unwrap())
                        .collect();

                    PaletteColor::new_from_vec(rgb)
                }).collect();

            let palette_number = match u32::from_str_radix(&xml_palette.number, 10) {
                Ok(s) => s,
                Err(e) => panic!(
                    "Failed to parse palette number -- {:?}: {}",
                    e, &xml_palette.number
                ),
            };

            Palette {
                number: palette_number,
                name: xml_palette.name.clone(),
                colors: palette_colors,
            }
        }).collect::<Vec<Palette>>();

    let pretty = ron::ser::PrettyConfig {
        depth_limit: 4,
        ..ron::ser::PrettyConfig::default()
    };

    let palettes_str = ron::ser::to_string_pretty(&palettes, pretty).unwrap();

    let path = Path::new("flam3-palettes.ron");

    let mut palettes_file = match File::create(&path) {
        Err(e) => panic!("couldn't open {}: {}", path.display(), e),
        Ok(file) => file,
    };

    let csv_path = Path::new("flam3-palettes.csv");

    let mut palettes_csv_file = match File::create(&csv_path) {
        Err(e) => panic!("couldn't open {}: {}", csv_path.display(), e),
        Ok(file) => file,
    };

    for palette in palettes {
        let colors_str = palette.colors.iter().map(|color| format!("{}, {}, {}", color.r, color.g, color.b)).fold(String::new(), | sum, val| format!("{}, {}", sum, val));
        let line_str = format!("{}, {} {}\n", palette.name, palette.number, colors_str);
        palettes_csv_file.write_all(line_str.as_bytes());
    }


    palettes_file
        .write_all(palettes_str.as_bytes())
        .expect("unable to write data");
}
