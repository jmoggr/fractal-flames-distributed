use std::f32::consts::PI;
use rand::Rng;
use rand::prelude::*;
use std::ops::{Mul};

#[derive(Clone, Serialize, Deserialize)]
pub struct AffineTransform2D {
    pub coefficients: [f32; 9],
}

impl AffineTransform2D {
    pub fn new_identity() -> AffineTransform2D {
        AffineTransform2D {
            coefficients:
                [1.0, 0.0, 0.0,
                 0.0, 1.0, 0.0,
                 0.0, 0.0, 1.0],
        }
    }

    pub fn new_rand(rng: &mut RngCore) -> AffineTransform2D {
        AffineTransform2D {
            coefficients: [
                rng.gen_range(-1.0, 1.0), rng.gen_range(-1.0, 1.0), rng.gen_range(-1.0, 1.0),
                rng.gen_range(-1.0, 1.0), rng.gen_range(-1.0, 1.0), rng.gen_range(-1.0, 1.0),
                rng.gen_range(-1.0, 1.0), rng.gen_range(-1.0, 1.0), rng.gen_range(-1.0, 1.0),],
        }
    }

    pub fn new_rotation(deg: f32) -> AffineTransform2D {
        let rad = (deg * PI)/180.0;
        AffineTransform2D {
            coefficients:
                [f32::cos(rad), -f32::sin(rad), 0.0,
                 f32::sin(rad), f32::cos(rad), 0.0,
                 0.0, 0.0, 1.0],
        }
    }

    pub fn new_translation(x: f32, y: f32) -> AffineTransform2D {
        AffineTransform2D {
            coefficients:
                [1.0, 0.0, x,
                 0.0, 1.0, y,
                 0.0, 0.0, 1.0],
        }
    }

    pub fn new_scale(scale: f32) -> AffineTransform2D {
        AffineTransform2D {
            coefficients:
                [scale, 0.0, 0.0,
                 0.0, scale, 0.0,
                 0.0, 0.0, 1.0],
        }
    }

    pub fn rotate(&self, deg: f32) -> AffineTransform2D {
        self * &AffineTransform2D::new_rotation(deg)
    }

    pub fn translate(&self, delta_x: f32, delta_y: f32) -> AffineTransform2D {
        self * &AffineTransform2D::new_translation(delta_x, delta_y)
    }
}

impl Mul for AffineTransform2D {
    type Output = AffineTransform2D;

    fn mul(self, other: AffineTransform2D) -> AffineTransform2D {
        let [a0, b0, c0, d0, e0, f0, g0, h0, i0] = self.coefficients;
        let [a1, b1, c1, d1, e1, f1, g1, h1, i1] = other.coefficients;
        AffineTransform2D {
            coefficients: [
                a0*a1 + b0*d1 + c0*g1, a0*b1 + b0*e1 + c0*h1, a0*c1 + b0*f1 + c0*i1,
                d0*a1 + e0*d1 + f0*g1, d0*b1 + e0*e1 + f0*h1, d0*c1 + e0*f1 + f0*i1,
                g0*a1 + h0*d1 + i0*g1, g0*b1 + h0*e1 + i0*h1, g0*c1 + h0*f1 + i0*i1,
            ]
        }
    }
}

impl<'a, 'b> Mul<&'b AffineTransform2D>  for &'a AffineTransform2D {
    type Output = AffineTransform2D;

    fn mul(self, other: &'b AffineTransform2D) -> AffineTransform2D {
        let [a0, b0, c0, d0, e0, f0, g0, h0, i0] = self.coefficients;
        let [a1, b1, c1, d1, e1, f1, g1, h1, i1] = other.coefficients;
        AffineTransform2D {
            coefficients: [
                a0*a1 + b0*d1 + c0*g1, a0*b1 + b0*e1 + c0*h1, a0*c1 + b0*f1 + c0*i1,
                d0*a1 + e0*d1 + f0*g1, d0*b1 + e0*e1 + f0*h1, d0*c1 + e0*f1 + f0*i1,
                g0*a1 + h0*d1 + i0*g1, g0*b1 + h0*e1 + i0*h1, g0*c1 + h0*f1 + i0*i1,
            ]
        }
    }
}
