use std::ops::{AddAssign, Add};
use rand::Rng;
use rand::prelude::*;

#[derive(Copy, Clone, Debug, Serialize, Deserialize)]
pub struct Color {
    pub r: f32,
    pub g: f32,
    pub b: f32,
}

impl Color {
    pub fn new(r: f32, g: f32, b: f32) -> Color {
        return Color { r: r, g: g, b: b };
    }

    pub fn new_zero() -> Color {
        return Color::new(0.0, 0.0, 0.0);
    }

    pub fn new_rand(rng: &mut RngCore) -> Color {
        return Color {
            r: rng.gen_range(0.0, 1.0),
            g: rng.gen_range(0.0, 1.0),
            b: rng.gen_range(0.0, 1.0),
        };
    }

    pub fn average_with_color(&self, color: Color) -> Color {
        return Color {
            r: (self.r + color.r) / 2.0,
            g: (self.g + color.g) / 2.0,
            b: (self.b + color.b) / 2.0,
        };
    }

    pub fn scale(&self, scale: f32) -> Color {
        return Color {
            r: self.r * scale,
            g: self.g * scale,
            b: self.b * scale,
        };
    }

    pub fn as_u8_slice(&self) -> [u8; 3] {
        let Color {
            mut r,
            mut g,
            mut b,
        } = self;

        if r > 1.0 {
            r = 1.0;
        }
        if g > 1.0 {
            g = 1.0;
        }
        if b > 1.0 {
            b = 1.0;
        }

        // if self.r > 1.0 || self.g > 1.0 || self.b > 1.0 {
        // let max = self.r.max(self.g.max(self.b));
        // return [((self.r/max) * 255.0) as u8, ((self.g/max) * 255.0) as u8, ((self.b/max) * 255.0) as u8];
        // } else {
        return [(r * 255.0) as u8, (g * 255.0) as u8, (b * 255.0) as u8];
        // }
    }
}

impl AddAssign for Color {
    fn add_assign(&mut self, rhs: Color) {
        self.r += rhs.r;
        self.g += rhs.g;
        self.b += rhs.b;
    }
}

impl Add for Color {
    type Output = Color;

    fn add(self, rhs: Color) -> Color {
        Color {
            r: self.r + rhs.r,
            g: self.g + rhs.g,
            b: self.b + rhs.b,
        }
    }
}

impl<'a, 'b> Add<&'b Color>  for &'a Color {
    type Output = Color;

    fn add(self, other: &'b Color) -> Color {
        Color {
            r: self.r + other.r,
            g: self.g + other.g,
            b: self.b + other.b,
        }
    }
}
