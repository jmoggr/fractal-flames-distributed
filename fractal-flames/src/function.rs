use ::point::Point;
use ::variants::Variant;
use ::affine_transform_2d::AffineTransform2D;
use ::variants::VARIATIONS;

#[derive(Clone, Serialize, Deserialize)]
pub struct Function {
    pub pre_transform: AffineTransform2D,
    pub post_transform: AffineTransform2D,
    pub variants: Vec<Variant>,
    pub weight: f32,
    pub color_index: f32,
    pub degs_per_tick: f32,
}

impl Function {
    // fn new() -> Function {
    //     Function {
    //         pre_transform: AffineTransform2D::new_identity(),
    //         post_transform: AffineTransform2D::new_identity(),
    //         variants: vec![],
    //         weight: 0.0,
    //         color_index: 0.0,
    //         degs_per_tick: 0.0,
    //     }
    // }

    fn apply_variations(&self, point: Point) -> Point {
        let mut new_point = Point::new(0.0, 0.0);

        for variant in &self.variants {
            let variation = VARIATIONS
                .iter()
                .find(|variation| variation.name == variant.name)
                .unwrap();
            new_point += (variation.function)(point).scale(variant.blending_factor);
        }

        return new_point;
    }

    pub fn transform_point(&self, point: &Point) -> Point {
        let pre_transform_point = point.transform(&self.pre_transform);
        let transform_point = self.apply_variations(pre_transform_point);
        let post_transform_point = transform_point.transform(&self.post_transform);

        return post_transform_point;
    }

    pub fn transform_color_index(&self, color_index: f32) -> f32 {
        return (self.color_index + color_index)/2.0;
    }
}
