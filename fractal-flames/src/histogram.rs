use std::ops::{Add, AddAssign};
use ::color::*;
use ::point::*;

fn dynamic_range(x: f32, a: f32, b: f32, min: f32, max: f32) -> f32 {
    ((b - a) * (x - min)) / (max - min) + a
}

#[derive(Clone, Serialize, Deserialize)]
pub struct HistogramBucket {
    pub color: Color,
    pub frequency: u32,
}

impl HistogramBucket {
    pub fn new() -> HistogramBucket {
        HistogramBucket {
            color: Color::new_zero(),
            frequency: 0,
        }
    }
}

impl AddAssign for HistogramBucket {
    fn add_assign(&mut self, other: HistogramBucket) {
        self.color += other.color;
        self.frequency += other.frequency;
    }
}

impl Add for HistogramBucket {
    type Output = HistogramBucket;

    fn add(self, other: HistogramBucket) -> HistogramBucket {
        HistogramBucket {
            color: self.color + other.color,
            frequency: self.frequency + other.frequency,
        }
    }
}

impl<'a, 'b> Add<&'b HistogramBucket>  for &'a HistogramBucket {
    type Output = HistogramBucket;

    fn add(self, other: &'b HistogramBucket) -> HistogramBucket {
        HistogramBucket {
            color: self.color + other.color,
            frequency: self.frequency + other.frequency,
        }
    }
}

#[derive(Clone, Serialize, Deserialize)]
pub struct Histogram {
    pub hist_width: u32,
    pub hist_height: u32,
    pub x_range_min: f32,
    pub x_range_max: f32,
    pub y_range_min: f32,
    pub y_range_max: f32,
    pub buckets: Vec<HistogramBucket>,
}

impl Histogram {
    pub fn new(
        hist_width: u32,
        hist_height: u32,
    ) -> Histogram {
        return Histogram {
            hist_width: hist_width,
            hist_height: hist_height,
            x_range_min: -10.0,
            x_range_max: 10.0,
            y_range_min: -10.0,
            y_range_max: 10.0,
            buckets: vec![HistogramBucket::new(); (hist_width * hist_height) as usize],
        };
    }

    pub fn do_ssaa(&self, ssaa_factor: u32) -> Histogram {
        let new_width = self.hist_width/ssaa_factor;
        let new_height = self.hist_height/ssaa_factor;

        let mut new_buckets = vec![HistogramBucket::new(); (new_width * new_height) as usize];

        for x in 0..new_width {
            for y in 0..new_height {
                let bucket_index = y * new_width + x;
                new_buckets[bucket_index as usize] = self.get_cell_average(x, y, ssaa_factor);;
            }
        }

        Histogram {
            hist_width: new_width,
            hist_height: new_height,
            buckets: new_buckets,
            ..*self
        }
    }

    fn get_cell_average(&self, x: u32, y: u32, ssaa_factor: u32) -> HistogramBucket {
        let mut bucket = HistogramBucket::new();
        for x in x * ssaa_factor..x * ssaa_factor + ssaa_factor {
            for y in y * ssaa_factor..y * ssaa_factor + ssaa_factor {
                let bucket_index = y * self.hist_width + x;

                bucket += self.buckets.get(bucket_index as usize).unwrap().clone();
            }
        }

        return bucket;
    }

    fn point_to_coord(&self, point: Point) -> (u32, u32) {
        let x = f32::floor(dynamic_range(
            point.x,
            0.0,
            (self.hist_width - 1) as f32,
            self.x_range_min,
            self.x_range_max,
        )) as u32;

        let y = f32::floor(dynamic_range(
            point.y,
            0.0,
            (self.hist_height - 1) as f32,
            self.y_range_min,
            self.y_range_max,
        )) as u32;

        return (x, y);
    }

    pub fn add_point(&mut self, point: Point, color: Color) -> bool {

        if point.x < self.x_range_max
            && point.x > self.x_range_min
            && point.y < self.y_range_max
            && point.y > self.y_range_min
        {
            let (x, y) = self.point_to_coord(point);
            // This check should always be true
            if y < self.hist_height && x < self.hist_width {
                let bucket_index = (x + y * self.hist_width) as usize;

                let hist_bucket = self.buckets.get_mut(bucket_index as usize).unwrap();
                hist_bucket.color += color;
                hist_bucket.frequency += 1;
            } else {
                return false;
            }

            return true;
        } else {
            return false;
        }
    }

    pub fn scale_colors(&self) -> Vec<Color> {
        self.buckets.iter().map(|bucket| {
            if bucket.frequency != 0 {
                let scale = f32::log(bucket.frequency as f32, 10.0)/bucket.frequency as f32;
                return bucket.color.scale(scale);
            } else {
                return bucket.color;
            }
        }).collect()
    }

    pub fn get_rgb_vec(&self) -> Vec<u8> {
        self.scale_colors().iter().flat_map(|color| color.as_u8_slice().to_vec())
        .collect()
    }

    pub fn get_rgba_vec(&self) -> Vec<u8> {
        self.scale_colors().iter().flat_map(|color| {
            let mut pixel = color.as_u8_slice().to_vec();
            pixel.push(255);
            return pixel;
        }).collect()
    }

    // pub fn write_file(&self, path: &str) {
    //     let colors = self.scale_colors();
    //     let data: Vec<u8> = colors
    //         .iter()
    //         .flat_map(|color| color.as_u8_slice().to_vec())
    //         .collect();

    //     let file = match File::create(&path) {
    //         Err(e) => panic!("couldn't open file {}: {}", path, e.description()),
    //         Ok(file) => file,
    //     };

    //     let ref mut w = BufWriter::new(file);

    //     let mut encoder = png::Encoder::new(w, self.hist_width as u32, self.hist_height as u32);
    //     encoder.set(png::ColorType::RGB).set(png::BitDepth::Eight);
    //     let mut writer = encoder.write_header().unwrap();

    //     writer.write_image_data(&data).unwrap();
    // }
}


impl Add for Histogram {
    type Output = Histogram;

    fn add(self, other: Histogram) -> Histogram {
        let summed_buckets = self.buckets.iter().zip(&other.buckets).map(|(sb, ob)| {
            sb + ob
        }).collect::<Vec<HistogramBucket>>();

        Histogram {
            buckets: summed_buckets,
            ..self
        }
    }
}

impl AddAssign for Histogram {
    fn add_assign(&mut self, other: Histogram) {
        self.buckets.iter_mut().zip(&other.buckets).for_each(|(sb, ob)| {
            sb.frequency += ob.frequency;
            sb.color += ob.color;
        });
        // self.buckets 
        // self.r += rhs.r;
        // self.g += rhs.g;
        // self.b += rhs.b;
    }
}
