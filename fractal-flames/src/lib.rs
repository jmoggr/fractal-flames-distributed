#![allow(dead_code)]

extern crate rand;
extern crate ron;
extern crate png;
extern crate rand_core;


#[macro_use]
extern crate serde;

pub mod scene;
pub mod runner;
mod point;
mod variants;
mod affine_transform_2d;
mod colormaps;
mod color;
mod function;
mod palette;
mod histogram;

use scene::*;
use histogram::*;

#[derive(Clone, Serialize, Deserialize)]
pub enum ToServerMsg {
    RequestWork,
    Beat(u64, u32),
    PartialScene(u64, ScenePartial),
}

#[derive(Clone, Serialize, Deserialize)]
pub enum ToClientMsg {
    NewScene(SceneDescription),
    Beat(f32, u32),
    RequestPartialScene,
}
