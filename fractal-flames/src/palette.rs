use ::color::Color;

#[derive(Clone, Debug, Serialize, Deserialize)]
pub struct Palette {
    pub number: u32,
    pub name: String,
    pub colors: Vec<Color>,
}

impl Palette {
    pub fn get_color_at_index(&self, index_float: f32) -> Color {
        let index = ((index_float * 255.0).round() as usize) % 255;
        self.colors[index]
    }

    pub fn from_u8(&mut self) {
        self.colors.iter_mut().for_each(|color| {
            color.r = color.r/255.0;
            color.g = color.g/255.0;
            color.b = color.b/255.0;
        });
    }
}
