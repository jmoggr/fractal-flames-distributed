use std::ops::{AddAssign};
use std::fmt;
use rand::Rng;
use rand::prelude::*;
use ::affine_transform_2d::*;

#[derive(Copy, Clone, Debug, Serialize, Deserialize)]
pub struct Point {
    pub x: f32,
    pub y: f32,
}

impl Point {
    pub fn new(x: f32, y: f32) -> Point {
        return Point { x: x, y: y };
    }

    pub fn new_rand(rng: &mut RngCore) -> Point {
        return Point::new(rng.gen_range(-1.0, 1.0), rng.gen_range(-1.0, 1.0));
    }

    pub fn transform(&self, matrix: &AffineTransform2D) -> Point {
        let [a, b, c, d, e, f, _, _, _] = matrix.coefficients;
        return Point::new(
            a * self.x + b * self.y + c,
            d * self.x + e * self.y + f,
        );
    }

    pub fn scale(&self, value: f32) -> Point {
        return Point::new(self.x * value, self.y * value);
    }
}

impl AddAssign for Point {
    fn add_assign(&mut self, rhs: Point) {
        self.x += rhs.x;
        self.y += rhs.y;
    }
}

impl fmt::Display for Point {
    fn fmt(&self, f: &mut fmt::Formatter) -> fmt::Result {
        return write!(f, "x: {}; y: {}", self.x, self.y);
    }
}
