extern crate rand_core;

use rand::prelude::*;

use ::affine_transform_2d::AffineTransform2D;
use ::variants::*;
use ::function::*;
use ::palette::*;
use ::scene::*;
use ::point::*;
use ::histogram::*;

pub struct Runner {
    pub point: Point,
    pub color_index: f32,
    pub histogram: Histogram,
    pub steps_taken: u32,
    pub miss_streak: u32,
    pub samples_taken: u32,
    pub quality: f32,
    pub ignore_steps: u32,
    pub scene_id: u64,
    pub functions: Vec<Function>,
    pub palette: Palette,
}

impl Runner {
    pub fn new(mut rng: &mut RngCore, scene: SceneDescription) -> Runner {
        Runner {
            scene_id: scene.id,
            point: Point::new_rand(&mut rng),
            histogram: Histogram::new(scene.width, scene.height),
            samples_taken: 0,
            miss_streak: 0,
            color_index: scene.starting_color_index,
            steps_taken: 0,
            quality: scene.quality,
            ignore_steps: scene.ignore_steps,
            functions: scene.functions,
            palette: scene.palette,
        }
    }

    pub fn get_partial_scene(&self) -> ScenePartial {
        ScenePartial {
            id: self.scene_id,
            histogram: self.histogram.clone(),
            samples_taken: self.samples_taken,
            steps_taken: self.steps_taken,
        }
    }

    pub fn run(&mut self, mut rng: &mut RngCore, num_steps_to_run: u32) {
        let num_buckets = self.histogram.hist_width * self.histogram.hist_height;

        let mut num_steps_run = 0;

        while num_steps_run < num_steps_to_run {
            let function_index = self.select_random_weighted_function(&mut rng);
            self.point = self.functions[function_index].transform_point(&self.point);

            self.color_index = self.functions[function_index].transform_color_index(self.color_index);
            // let fcolor = color.average_with_color(color_final);
            let color = self.palette.get_color_at_index(self.color_index);

            // color = function.transform_color(color);
            // let fcolor = color.average_with_color(color_final);

            let samples_per_bucket = self.samples_taken as f32 / num_buckets as f32;

            if samples_per_bucket > self.quality {
                break;
            }

            if self.steps_taken > self.ignore_steps {
                if self.histogram.add_point(self.point, color) {
                    self.samples_taken += 1;
                    self.miss_streak = 0;

                } else {
                    self.miss_streak += 1;

                    if self.miss_streak > 100 {
                        println!("ERROR: system is not converging, stoping iterations");
                        break;
                    }
                }
            }

            num_steps_run += 1;
            self.steps_taken += 1;
        }
    }

    fn select_random_weighted_function(&self, rng: &mut RngCore) -> usize {
        let rand: f32 = rng.gen_range(0.0, 1.0);
        let mut weight = 0.0;

        let mut i = 0;

        for function in &self.functions {
            weight += function.weight;
            if rand <= weight {
                break;
            }

            i += 1;
        }

        if i == self.functions.len() {
            i = 0;
        }

        return i;
        // return &self.functions[i];
    }
}
