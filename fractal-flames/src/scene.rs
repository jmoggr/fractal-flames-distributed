extern crate ron;
extern crate png;
extern crate rand_core;
extern crate rand;
extern crate chrono;

use std::fs;
use std::io::Write;
use png::HasParameters;
use std::io::BufWriter;
use rand::thread_rng;
use rand::Rng;
use std::ops::Range;
use rand::seq;
use std::path::Path;
use rand::prelude::*;
use colormaps::PARULA;
use color::*;
use histogram::*;
use std::fs::File;
use std::error::Error;
use std::f32::consts::PI;


use ::affine_transform_2d::AffineTransform2D;
use ::variants::Variant;
use ::variants::Variation;
use ::variants::VARIATIONS;
use ::function::Function;
use ::palette::Palette;
use std::ops::Add;

#[derive(Clone, Serialize, Deserialize)]
pub struct Scene {
    pub id: u64,
    pub functions: Vec<Function>,
    pub palette: Palette,
    pub background_color_index: f32,
    pub starting_color_index: f32,
    pub camera_transform: AffineTransform2D,
    pub width: u32,
    pub height: u32,
    pub histogram: Histogram,
    pub quality: f32,
    ssaa_factor: u32,
    steps_taken: u32,
    samples_taken: u32,
    ignore_steps: u32,
}

#[derive(Clone, Serialize, Deserialize)]
pub struct ScenePartial {
    pub id: u64,
    pub histogram: Histogram,
    pub samples_taken: u32,
    pub steps_taken: u32,
}

#[derive(Clone, Serialize, Deserialize)]
pub struct SceneDescription {
    pub id: u64,
    pub functions: Vec<Function>,
    pub palette: Palette,
    pub background_color_index: f32,
    pub starting_color_index: f32,
    pub camera_transform: AffineTransform2D,
    pub quality: f32,
    pub width: u32,
    pub height: u32,
    pub ignore_steps: u32,
}


impl Scene {
    // fn from_file(scene_filepath: &Path) -> Result<Scene, String> {
    //     let scene_file = File::open(&scene_filepath).map_err(|e| e.to_string())?;
    //     ron::de::from_reader::<File, Scene>(scene_file).map_err(|e| e.to_string())
    // }

    pub fn new_random(width: u32, height: u32, quality: f32, ssaa_factor: u32) -> Scene {
        let palettes_filepath = Path::new("/work/jason/projects/fractal-flames-distributed/flam3-palettes.ron");
        let mut rng = thread_rng();

        let num_variations = 5;

        let functions = Scene::create_random_system(&mut rng, 8..20, 1..num_variations, 1..num_variations, 5..10);

        let palettes_file = File::open(palettes_filepath).expect("could no open palettes file");

        let mut palettes: Vec<Palette> = match ron::de::from_reader(palettes_file) {
            Ok(x) => x,
            Err(e) => {
                println!("Failed to load palettes: {}", e);
                ::std::process::exit(1);
            }
        };

        for palette in palettes.iter_mut() {
            palette.from_u8();
        }

        let palette = rng.choose(&palettes).unwrap();
        let background_color_index: f32 = rng.gen_range(0.0, palette.colors.len() as f32);
        let starting_color_index: f32 = rng.gen_range(0.0, palette.colors.len() as f32);

        let ssaa_width = width * ssaa_factor;
        let ssaa_height = height * ssaa_factor;
        let histogram = Histogram::new(ssaa_width, ssaa_height);

        Scene {
            id: rand::random::<u64>(),
            functions: functions,
            palette: palette.clone(),
            background_color_index: background_color_index,
            starting_color_index: starting_color_index,
            camera_transform: AffineTransform2D::new_identity(),
            histogram: histogram,
            width: width,
            height: height,
            ssaa_factor: ssaa_factor,
            quality: quality,
            ignore_steps: 20,
            steps_taken: 0,
            samples_taken: 0,
        }
    }

    pub fn get_description(&self) -> SceneDescription {
        SceneDescription {
            id: self.id,
            quality: self.quality,
            functions: self.functions.clone(),
            camera_transform: self.camera_transform.clone(),
            background_color_index: self.background_color_index,
            starting_color_index: self.starting_color_index,
            palette: self.palette.clone(),
            width: self.width,
            height: self.height,
            ignore_steps: self.ignore_steps,
        }
    }

    pub fn create_random_system(
        mut rng: &mut RngCore,
        num_functions_range: Range<i32>,
        num_variants_range: Range<i32>,
        num_varients_per_function_range: Range<i32>,
        degs_per_tick_range: Range<i32>,
    ) -> Vec<Function> {
        let num_functions = rng.gen_range(num_functions_range.start, num_functions_range.end);
        let num_variants = rng.gen_range(num_variants_range.start, num_variants_range.end);

        let variations: Vec<&Variation> =
            seq::sample_iter(&mut rng, VARIATIONS.iter(), num_variants as usize).unwrap();
        let function_weights = create_normalized_weights(&mut rng, num_functions as usize);

        let mut functions: Vec<Function> = vec![];

        for function_weight in function_weights {
            let mut num_variants_in_function =
                seq::sample_iter(&mut rng, num_varients_per_function_range.clone(), 1).unwrap()[0];

            let degs_per_tick = 1.0; //seq::sample_iter(&mut rng, degs_per_tick_range.clone(), 1).unwrap()[0] as f32;

            if num_variants_in_function > variations.len() as i32 {
                num_variants_in_function = (variations.len()) as i32;
            }

            let variation_names: Vec<&'static str> = seq::sample_iter(
                &mut rng,
                variations.clone(),
                num_variants_in_function as usize,
            ).unwrap()
                .iter()
                .map(|variation| variation.name)
                .collect();

            let variant_blending_factors =
                create_normalized_weights(&mut rng, num_variants_in_function as usize);

            let variants: Vec<Variant> = variation_names
                .iter()
                .zip(variant_blending_factors)
                .map(|(variation_name, blending_factor)| Variant {
                    name: String::from(*variation_name),
                    blending_factor: blending_factor,
                })
                .collect();

            let color_index = rng.gen_range(0.0, 1.0);

            let pre_transform = AffineTransform2D::new_rand(&mut rng);
            let post_transform = AffineTransform2D::new_rand(&mut rng);

            let function = Function {
                variants: variants,
                pre_transform: pre_transform,
                post_transform: post_transform,
                weight: function_weight,
                color_index: color_index,
                degs_per_tick: degs_per_tick,
            };

            functions.push(function);
        }

        return functions;
    }

    pub fn do_ssaa(&mut self) {
        self.histogram = self.histogram.do_ssaa(self.ssaa_factor);
    }

    pub fn write_to_file(&self, filepath: String) {
        let mut outfile = match File::create(&filepath) {
            Err(e) => panic!("couldn't open {}: {}", filepath, e),
            Ok(file) => file,
        };

        let pretty = ron::ser::PrettyConfig {
            depth_limit: 5,
            ..ron::ser::PrettyConfig::default()
        };

        let scene_str = ron::ser::to_string_pretty(&self, pretty).unwrap();
        outfile.write_all(scene_str.as_bytes()).expect("unable to write data");
    }

    pub fn render(&self) {
        println!("starting processing");
        let width = 1280;
        let height = 720;
        let kernel_size = 5 as u32;
        let sigma = 1.0;
        let epanechnikov_kernel = get_epanechnikov_kernel(kernel_size, sigma);
        let frequencies = self.histogram.buckets.iter().map(|bucket| bucket.frequency as f32).collect();

        let mut density = convolve2d(frequencies, width, height, epanechnikov_kernel, kernel_size);


        for x in &mut density {
            *x = f32::log(*x + 0.001, 10.0);
        }

        let min_density = density.iter().cloned().fold(0./0., f32::min);
        let max_density = density.iter().cloned().fold(0./0., f32::max);

        for x in &mut density {
            *x = (*x - min_density)/(max_density - min_density);
        }

        let now = chrono::offset::Local::now();
        let dirname = format!("results/{}", now.format("%Y-%m-%d %H:%M:%S"));
        fs::create_dir(&dirname).unwrap();

        let scene_path = format!("{}/scene.ron", &dirname);
        self.write_to_file(scene_path);
        let density_path = format!("{}/density-estimation.png", &dirname);
        print_density(&density, width, height, &density_path);

        let colors = self.histogram.scale_colors();
        let max_blur_radius = 5 as usize;
        let new_colors = blur(colors, density, width, height, max_blur_radius);
        let data: Vec<u8> = new_colors
                .iter()
                .flat_map(|color| color.as_u8_slice().to_vec())
                .collect();

        let flame_path = format!("{}/flame.png", &dirname);

        let file = match File::create(&flame_path) {
            Err(e) => panic!("couldn't open file {}: {}", flame_path, e.description()),
            Ok(file) => file,
        };

        let ref mut w = BufWriter::new(file);

        let mut encoder = png::Encoder::new(w, width as u32, height as u32);
        encoder.set(png::ColorType::RGB).set(png::BitDepth::Eight);
        let mut writer = encoder.write_header().unwrap();

        writer.write_image_data(&data).unwrap();
        println!("finished processing");
    }

    pub fn add_partial(&mut self, other: ScenePartial) {
        self.histogram += other.histogram;
        self.samples_taken += other.samples_taken;
        self.steps_taken += other.steps_taken;
    }
}

impl Add for Scene {
    type Output = Scene;

    fn add(self, other: Scene) -> Scene {
        if self.id != other.id {
            panic!("scenes not same");
        }

        Scene {
            histogram: self.histogram + other.histogram,
            steps_taken: self.steps_taken + other.steps_taken,
            samples_taken: self.samples_taken + other.samples_taken,
            ..self
        }
    }
}

fn convolve2d(mat: Vec<f32>, width: u32, height: u32, kernel: Vec<f32>, kernel_size: u32) -> Vec<f32> {
    let mut result = vec![0.0; (width * height) as usize];
    let half = (kernel_size as f32/ 2.0).floor();

    for x in 0..width {
        for y in 0..height {
            let x_start = if x as i32 - (half as i32) < 0 {
                0
            } else {
                x - half as u32
            };

            let y_start = if y as i32 - (half as i32) < 0 {
                0
            } else {
                y - half as u32
            };

            let x_end = if x + (half as u32) >= width {
                width - 1
            } else {
                x + half as u32 + 1
            };

            let y_end = if y + (half as u32) >= height {
                height - 1
            } else {
                y + half as u32 + 1
            };

            let mut sum = 0.0;

            for (kx, x1) in (x_start..x_end).enumerate() {
                for (ky, y1) in (y_start..y_end).enumerate() {
                    let input_index = (x1 as u32 + y1 as u32 * width) as usize;
                    let kernel_index = (kx as u32 + ky as u32 * kernel_size) as usize;
                    sum += mat[input_index] as f32 * kernel[kernel_index];
                }
            }

            result[(x + y*width) as usize] = sum;
        }
    }

    return result;
}

// https://math.stackexchange.com/questions/41175/2d-epanechnikov-kernel
// https://math.stackexchange.com/questions/40922/epanechnikov-kernel-with-variable-width
fn get_epanechnikov_kernel(kernel_size: u32, sigma: f32) -> Vec<f32> {
    let mut sum = 0.0;
    let mut kernel = vec![0.0; (kernel_size * kernel_size) as usize];
    let half = (kernel_size as f32/ 2.0).floor();

    for x in 0..kernel_size {
        for y in 0..kernel_size {

            let xt = (x as f32 - half)/half;
            let yt = (y as f32 - half)/half;

            let u_sqr = ((xt) / sigma).powf(2.0) + ((yt) / sigma).powf(2.0);

            if u_sqr.abs() > 1.0 {
                continue;
            }

            let index = x + kernel_size * y;
            let value = (2.0/(PI * sigma * sigma)) * (1.0 - u_sqr);
            kernel[index as usize] = value;
            sum += value;
        }
    }

    for x in &mut kernel {
        *x /= sum;
    }

    return kernel;
}


fn gaussian(x: f32, mu: f32, sigma: f32) -> f32 {
    (-(((x - mu)/(sigma)).powf(2.0))/2.0).exp()
}

fn get_gaussian_kernel(radius: u32, sigma: f32) -> Vec<f32> {
    let mut sum = 0.0;
    let kernel_size = radius * 2 + 1;
    let mut kernel = vec![0.0; (kernel_size * kernel_size) as usize];

    for x in 0..kernel_size {
        for y in 0..kernel_size {
            let value = gaussian(x as f32, radius as f32, sigma) * gaussian(y as f32, radius as f32, sigma);

            let index = x + kernel_size * y;
            kernel[index as usize] = value;
            sum += value;
        }
    }

    for x in &mut kernel {
        *x /= sum;
    }

    return kernel;
}

fn print_density(density: &Vec<f32>, width: u32, height: u32, filename: &str) {
    let pixels1 = density.iter().map(|d| {
        let den = d * 62.0;
        let r_factor = den - den.floor();
        let l_factor = 1.0 - r_factor;
        let left_c = PARULA[den as usize];
        let right_c = PARULA[den as usize + 1];

        vec![left_c[0] * l_factor + right_c[0] * r_factor,
        left_c[1] * l_factor + right_c[1] * r_factor,
        left_c[2] * l_factor + right_c[2] * r_factor]

    }).collect::<Vec<Vec<f32>>>();

    let pixels = pixels1.iter().flat_map(|p| p.iter().map(|x| (x * 255.0) as u8)).collect::<Vec<u8>>();

    // let pixels = density.iter().flat_map(|den| {
    //     parula[*den as usize].iter().map(|x| (x * 255.0) as u8)
    // }).collect::<Vec<u8>>();

    let file = match File::create(&filename) {
        Err(e) => panic!("couldn't open file {}: {}", filename, e.description()),
        Ok(file) => file,
    };

    let ref mut w = BufWriter::new(file);

    let mut encoder = png::Encoder::new(w, width as u32, height as u32);
    encoder.set(png::ColorType::RGB).set(png::BitDepth::Eight);
    let mut writer = encoder.write_header().unwrap();

    writer.write_image_data(&pixels).unwrap();
}


fn blur(colors: Vec<Color>, dens: Vec<f32>, width: u32, height: u32, max_blur_radius: usize) -> Vec<Color> {
    let density = dens.iter().map(|x| x * max_blur_radius as f32).collect::<Vec<f32>>();
    let kernels = (1..max_blur_radius + 1).map(|x| get_gaussian_kernel(x as u32, x as f32/2.0)).collect::<Vec<Vec<f32>>>();
    let mut new_colors: Vec<Color> = colors.iter().map(|_| Color::new_zero()).collect();

    for x in 0..width {
        for y in 0..height {
            let index = (x + y * width) as usize;
            let r = max_blur_radius as f32 - (density[index]);

            if r <= 1.0 {
                new_colors[index] = colors[index];
                continue;
            } 

            let radius = r.floor() as u32;
            let kernel_size = radius * 2 + 1;
            let kernel = &kernels[radius as usize - 1];

            let x_start = if x as i32 - (radius as i32) < 0 { 0 } else { x - radius };
            let y_start = if y as i32 - (radius as i32) < 0 { 0 } else { y - radius };
            let x_end = if x + radius >= width { width - 1 } else { x + radius + 1 };
            let y_end = if y + radius >= height { height - 1 } else { y + radius + 1 };

            for (kx, x1) in (x_start..x_end).enumerate() {
                for (ky, y1) in (y_start..y_end).enumerate() {
                    let color_index = (x1 as u32 + y1 as u32 * width) as usize;
                    let kernel_index = (kx as u32 + ky as u32 * kernel_size) as usize;

                    new_colors[index].r += colors[color_index].r * kernel[kernel_index];
                    new_colors[index].g += colors[color_index].g * kernel[kernel_index];
                    new_colors[index].b += colors[color_index].b * kernel[kernel_index];
                }
            }
        }
    }

    return new_colors;
}

fn create_normalized_weights(mut rng: &mut RngCore, num_weights: usize) -> Vec<f32> {
    let standard_distribution: Vec<f32> = 
        rand::distributions::Standard.sample_iter(&mut rng)
        .take(num_weights)
        .collect();
    let distribution_sum: f32 = standard_distribution.iter().sum();
    let normalized_distribution: Vec<f32> = standard_distribution
        .iter()
        .map(|value| value / distribution_sum)
        .collect();

    return normalized_distribution;
}
