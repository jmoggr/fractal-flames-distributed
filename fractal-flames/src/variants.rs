use ::point::*;
use std::f32::consts::PI;

#[derive(Debug)]
pub struct Variation {
    pub name: &'static str,
    pub function: fn(Point) -> Point,
}

#[derive(Clone, Debug, Serialize, Deserialize)]
pub struct Variant {
    pub blending_factor: f32,
    pub name: String,
}

pub const VARIATIONS: [Variation; 11] = [
    Variation {
        name: "linear",
        function: linear,
    },
    Variation {
        name: "sinusoidal",
        function: sinusoidal,
    },
    Variation {
        name: "spherical",
        function: spherical,
    },
    Variation {
        name: "swirl",
        function: swirl,
    },
    Variation {
        name: "horseshoe",
        function: horseshoe,
    },
    Variation {
        name: "polar",
        function: polar,
    },
    Variation {
        name: "hankerchief",
        function: hankerchief,
    },
    Variation {
        name: "heart",
        function: heart,
    },
    Variation {
        name: "disc",
        function: disc,
    },
    Variation {
        name: "spiral",
        function: spiral,
    },
    Variation {
        name: "hyperbolic",
        function: hyperbolic,
    },
];

pub fn linear(point: Point) -> Point {
    let new_x = point.x;
    let new_y = point.y;

    return Point::new(new_x, new_y);
}

pub fn sinusoidal(point: Point) -> Point {
    let new_x = point.x.sin();
    let new_y = point.y.sin();

    return Point::new(new_x, new_y);
}

pub fn spherical(point: Point) -> Point {
    let r = f32::sqrt(point.x * point.x + point.y * point.y);
    let new_x = point.x / (r * r);
    let new_y = point.y / (r * r);

    return Point::new(new_x, new_y);
}

pub fn swirl(point: Point) -> Point {
    let r = f32::sqrt(point.x * point.x + point.y * point.y);
    let new_x = f32::sin(r * r) * point.x - f32::cos(r * r) * point.y;
    let new_y = f32::cos(r * r) * point.x + f32::sin(r * r) * point.y;

    return Point::new(new_x, new_y);
}

pub fn horseshoe(point: Point) -> Point {
    let r = f32::sqrt(point.x * point.x + point.y * point.y);
    let new_x = ((point.x - point.y) * (point.x + point.y)) / r;
    let new_y = (2.0 * point.x * point.y) / r;

    return Point::new(new_x, new_y);
}

pub fn polar(point: Point) -> Point {
    let r = f32::sqrt(point.x * point.x + point.y * point.y);
    let a = f32::atan(point.x / point.y);

    let new_x = a / PI;
    let new_y = r - 1.0;

    return Point::new(new_x, new_y);
}

pub fn hankerchief(point: Point) -> Point {
    let r = f32::sqrt(point.x * point.x + point.y * point.y);
    let a = f32::atan(point.x / point.y);

    let new_x = r * (f32::sin(a + r));
    let new_y = r * (f32::cos(a - r));

    return Point::new(new_x, new_y);
}

pub fn heart(point: Point) -> Point {
    let r = f32::sqrt(point.x * point.x + point.y * point.y);
    let a = f32::atan(point.x / point.y);

    let new_x = r * (f32::sin(a * r));
    let new_y = r * -(f32::cos(a * r));

    return Point::new(new_x, new_y);
}

pub fn disc(point: Point) -> Point {
    let r = f32::sqrt(point.x * point.x + point.y * point.y);
    let a = f32::atan(point.x / point.y);

    let new_x = (PI/a) * (f32::sin(PI * r));
    let new_y = (PI/a) * (f32::cos(PI * r));

    return Point::new(new_x, new_y);
}

pub fn spiral(point: Point) -> Point {
    let r = f32::sqrt(point.x * point.x + point.y * point.y);
    let a = f32::atan(point.x / point.y);

    let new_x = (1.0/r) * (f32::cos(a) + f32::sin(r));
    let new_y = (1.0/r) * (f32::sin(a) - f32::cos(r));

    return Point::new(new_x, new_y);
}

pub fn hyperbolic(point: Point) -> Point {
    let r = f32::sqrt(point.x * point.x + point.y * point.y);
    let a = f32::atan(point.x / point.y);

    let new_x = f32::sin(a) / r;
    let new_y = r * f32::cos(a);

    return Point::new(new_x, new_y);
}
