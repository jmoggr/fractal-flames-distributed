extern crate ws;
extern crate fractal_flames;
extern crate rand;
extern crate ron;
extern crate bincode;

use fractal_flames::*;
use fractal_flames::scene::*;

use bincode::{serialize, deserialize};
use std::collections::HashMap;

use ws::{listen, Handler, Sender, Message, Handshake, CloseCode};
use ws::Result as WsResult;
use ws::util::Token;

use std::time::Instant;
use std::time::Duration;
use std::sync::mpsc;
use std::thread;
use std::sync::mpsc::Sender as ThreadSend;
use std::sync::mpsc::Receiver as ThreadRecv;

struct Server {
    out: Sender,
    manager_send: ThreadSend<ToManagerMsg>,
}

enum ToManagerMsg {
    PartialScene(Sender, u64, ScenePartial),
    Register(Sender),
    Beat(Sender, u64, u32),
    RendererDoneDistributing(u32),
    RendererDoneAccumulating(u32),
}

enum ToRendererMsg {
    Register(Sender),
    PartialScene(Sender, u64, ScenePartial),
    Beat(Sender, u64, u32),
}


impl Server {
    pub fn new(out: Sender, manager_send: ThreadSend<ToManagerMsg>) -> Server {
        Server {
            out: out,
            manager_send: manager_send,
        }
    }

    fn handle_msg(&self, msg: ToServerMsg) {
        match msg {
            ToServerMsg::RequestWork => {
                println!("client requesting work");
                self.manager_send.send(ToManagerMsg::Register(self.out.clone())).unwrap();
            },
            ToServerMsg::Beat(scene_id, steps_taken) => {
                println!("client sending beat");
                self.manager_send.send(ToManagerMsg::Beat(self.out.clone(), scene_id, steps_taken)).unwrap();
            },
            ToServerMsg::PartialScene(scene_id, partial_scene) => {
                println!("client sending histo data");
                self.manager_send.send(ToManagerMsg::PartialScene(self.out.clone(), scene_id, partial_scene)).unwrap();
            },
        }
    }
}

impl Handler for Server {
    fn on_open(&mut self, _: Handshake) -> WsResult<()> {
        println!("client connected");
        Ok(())
    }

    fn on_message(&mut self, msg: Message) -> WsResult<()> {
        self.handle_msg(deserialize(&msg.into_data()).unwrap());
        Ok(())
    }

    fn on_close(&mut self, code: CloseCode, reason: &str) {
        match code {
            CloseCode::Normal => println!("Client closed connection"),
            CloseCode::Away   => println!("Client left site"),
            CloseCode::Abnormal => println!("Client close failed"),
            _ => println!("Client connection error: {}", reason),
        }
    }

    fn on_error(&mut self, err: ws::Error) {
        println!("The server encountered an error: {:?}", err);
    }
}

struct Manager {
    send: ThreadSend<ToManagerMsg>,
    recv: ThreadRecv<ToManagerMsg>,
    current_renderer_id: u32,
    socket_renderer: HashMap<Token, u32>,
    renderer_senders: HashMap<u32, ThreadSend<ToRendererMsg>>,
}

impl Manager {
    fn new() -> Manager {
        let (send, recv): (ThreadSend<ToManagerMsg>, ThreadRecv<ToManagerMsg>) = mpsc::channel();

        let mut manager = Manager {
            send: send,
            recv: recv,
            current_renderer_id: 0,
            socket_renderer: HashMap::new(),
            renderer_senders: HashMap::new(),
        };

        manager.new_current_renderer();

        return manager;
    }

    fn new_current_renderer(&mut self) {
        self.current_renderer_id += 1;

        let mut renderer = Renderer::new(self.send.clone(), self.current_renderer_id);
        self.renderer_senders.insert(renderer.id, renderer.send.clone());

        thread::spawn(move || {
            renderer.start();
        });
    }

    // should just take the ws token
    fn socket_send_renderer_msg(&self, ws: Sender, msg: ToRendererMsg) -> Result<(), &'static str> {
        match self.socket_renderer.get(&ws.token()) {
            Some(renderer_id) => {
                match self.renderer_senders.get(renderer_id) {
                    Some(renderer_send) => {
                        match renderer_send.send(msg) {
                            Ok(_) => Ok(()),
                            Err(_) => Err("renderer channel send failed"),
                        }
                    },
                    None => Err("renderer has no send channel"),
                }
            },
            None => Err("socket sending beat has no renderer id"),
        }
    }

    fn is_socket_registered_current(&self, ws: Sender) -> bool {
        match self.socket_renderer.get(&ws.token()) {
            Some(socket_renderer_id) => {
                if *socket_renderer_id == self.current_renderer_id {
                    true
                } else {
                    false
                }
            },
            None => false,
        }
    }

    fn register_socket(&mut self, ws: Sender) -> Result<(), &'static str> {
        if self.is_socket_registered_current(ws.clone()) {
            println!("WARNING -- socket re registering even though it is already registered with teh current renderer");
            Ok(())
        } else {
            match self.renderer_senders.get(&self.current_renderer_id) {
                Some(current_renderer_send) => match current_renderer_send.send(ToRendererMsg::Register(ws.clone())) {
                    Ok(_) => {
                        self.socket_renderer.insert(ws.token(), self.current_renderer_id);
                        Ok(())
                    },
                    Err(_) => Err("could not send register with current renderer"),
                },
                None => Err("could not get current renderer send"),
            }
        }
    }

    fn handle_msg(&mut self, msg: ToManagerMsg) {
        match msg {
            ToManagerMsg::Register(ws) => {
                match self.register_socket(ws) {
                    Ok(_) => (),
                    Err(reason) => panic!("could not register socket {}", reason),
                }
            },
            ToManagerMsg::Beat(ws, steps_taken, scene_id) => {
                match self.socket_send_renderer_msg(ws.clone(), ToRendererMsg::Beat(ws.clone(), steps_taken, scene_id)) {
                    Ok(_) => (),
                    Err(reason) => {
                        println!("WARNING -- could not send beat to renderer: {}", reason);
                        self.send.send(ToManagerMsg::Register(ws)).unwrap();
                    },
                }
            },
            ToManagerMsg::PartialScene(ws, scene_id, partial_scene) => {
                match self.socket_send_renderer_msg(ws.clone(), ToRendererMsg::PartialScene(ws.clone(), scene_id, partial_scene)) {
                    Ok(_) => (),
                    Err(reason) => {
                        println!("WARNING -- could not send histo to renderer: {}", reason);
                        self.send.send(ToManagerMsg::Register(ws)).unwrap();
                    },
                };
            },
            ToManagerMsg::RendererDoneDistributing(renderer_id) => {
                if self.current_renderer_id == renderer_id {
                    self.new_current_renderer();
                } else {
                    panic!("ERROR -- Position reached that should be logically impossible");
                }
            },
            ToManagerMsg::RendererDoneAccumulating(renderer_id) => {
                if self.current_renderer_id == renderer_id {
                    panic!("ERROR -- Position reached that should be logically impossible");
                } else {
                    // TODO check if any clients still hold a reference to this render
                    self.renderer_senders.remove(&renderer_id);
                }
            },
        };
    }

    fn start(&mut self) {
        self.current_renderer_id += 1;
        let mut renderer = Renderer::new(self.send.clone(), self.current_renderer_id);
        self.renderer_senders.insert(renderer.id, renderer.send.clone());

        thread::spawn(move || {
            renderer.start();
        });

        loop {
            match self.recv.recv() {
                Ok(msg) => self.handle_msg(msg),
                Err(_) => panic!("Manager could not receive, not possible"),
            };
        }
    }
}

struct Renderer {
    clients: Vec<Sender>,
    num_points: u32,
    target_points: u32,
    scene: Scene,
    scene_description: SceneDescription,
    manager_send: ThreadSend<ToManagerMsg>,
    send: ThreadSend<ToRendererMsg>,
    recv: ThreadRecv<ToRendererMsg>,
    id: u32,
}

impl Renderer {
    pub fn new(manager_send: ThreadSend<ToManagerMsg>, id: u32) -> Renderer {

        let (send, recv): (ThreadSend<ToRendererMsg>, ThreadRecv<ToRendererMsg>) = mpsc::channel();

        let scene = Scene::new_random(1280, 720, 10.0, 1);
        let scene_description = scene.get_description();

        Renderer {
            id: id,
            clients: vec![],
            num_points: 0,
            target_points: 1000000,
            recv: recv,
            send: send,
            manager_send: manager_send,
            scene: scene,
            scene_description: scene_description,
        }
    }

    fn register(&mut self, ws: Sender) {
        if ! self.clients.iter().any(|ref client| client.token() == ws.token() ) {
            self.clients.push(ws.clone());
        }

        println!("sent scene to client");
        let msg = ToClientMsg::NewScene(self.scene_description.clone());
        ws.send(serialize(&msg).expect("Serialization failed")).expect("failed to send scene");
    }

    //TODO should be socket token
    fn verify_socket(&self, ws: Sender, scene_id: u64) -> Result<(), &'static str> {
        if self.scene.id != scene_id {
            Err("bad scene id")
        } else {
            if self.clients.iter().any(|ref client| client.token() == ws.token() ) {
                Ok(())
            } else {
                Err("socket not registered")
            }
        }
    }

    fn handle_msg_distributing(&mut self, msg: ToRendererMsg) {
        match msg {
            ToRendererMsg::Register(ws) => {
                self.register(ws);
            },
            ToRendererMsg::Beat(ws, scene_id, steps_taken) => {
                match self.verify_socket(ws.clone(), scene_id) {
                    Ok(_) => {
                        self.num_points += steps_taken;
                        let progress = self.num_points as f32 / self.target_points as f32;

                        let msg = ToClientMsg::Beat(progress, self.clients.len() as u32);
                        ws.send(serialize(&msg).expect("Serialization failed")).expect("failed to send beat");

                        if self.num_points > self.target_points {
                            break;
                        }
                    },
                    Err(reason) => {
                        println!("failed to read renderer beat, bad match: {}", reason);
                        self.manager_send.send(ToManagerMsg::Register(ws)).unwrap();
                    }
                }
            },
            ToRendererMsg::PartialScene(_, _, _) => {
                println!("WARNING -- premature reception of partial scene");
            },
        };
    }

    fn handle_msg_accumulating(&mut self, msg: ToRendererMsg) {
        match msg {
            ToRendererMsg::PartialScene(ws, scene_id, partial_scene) => {
                match self.verify_socket(ws.clone(), scene_id) {
                    Ok(_) => {
                        self.scene.add_partial(partial_scene);
                        println!("got histo data");
                    },
                    Err(reason) => {
                        println!("bad socket to renderer during accumulation {}", reason);
                    },
                };

                self.manager_send.send(ToManagerMsg::Register(ws)).unwrap();
            },
            _ => {
                println!("bad msg to renderer to renderer during accumulation, was expecting partial scene, ignoring");
            },
        };
    }

    fn start(&mut self) {
        loop {
            match self.recv.recv() {
                Ok(msg) => self.handle_msg_distributing(msg),
                Err(_) => panic!("could not receive distributing msg"),
            }
        }

        self.manager_send.send(ToManagerMsg::RendererDoneDistributing(self.id)).unwrap();

        println!("requesting histo data");
        for client in &self.clients {
            client.send(serialize(&ToClientMsg::RequestPartialScene).unwrap()).unwrap();
        }

        let mut acc_period = Duration::new(10, 0);
        loop {
            let timeout_start = Instant::now();
            match self.recv.recv_timeout(acc_period) {
                Ok(msg) => {
                    self.handle_msg_accumulating(msg);
                },
                Err(_) => break,
            }

            acc_period -= timeout_start.elapsed();
        }

        self.manager_send.send(ToManagerMsg::RendererDoneAccumulating(self.id)).unwrap();

        // Any clients that reach this point should be re-registered, if they have not come through
        // with the data then they will be bounced and re-registered
        drop(&self.recv);

        self.scene.render();
    }
}


fn main() {
    let mut manager = Manager::new();
    let sender = manager.send.clone();

    thread::spawn(move || {
        manager.start();
    });

    listen("0.0.0.0:3012", |out| Server::new(out, sender.clone())).unwrap()
} 
