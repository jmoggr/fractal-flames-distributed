extern crate ron;
extern crate fractal_flames;
extern crate rand;
extern crate bincode;

extern crate serde;
#[macro_use]
extern crate stdweb;

use bincode::{serialize, deserialize};

use stdweb::web::SocketBinaryType;
use std::cell::RefCell;
use fractal_flames::*;
use fractal_flames::scene::*;
use fractal_flames::runner::*;
use std::rc::Rc;
use rand::prelude::*;
use stdweb::traits::*;
use stdweb::unstable::TryInto;
use stdweb::web::{
    document,
    window,
    CanvasRenderingContext2d,
    WebSocket,
    HtmlElement,
};

use stdweb::web::html_element::InputElement;

use stdweb::web::event::{
    KeyPressEvent,
    MouseMoveEvent,
    ResizeEvent,
    SocketOpenEvent,
    SocketCloseEvent,
    SocketErrorEvent,
    SocketMessageEvent,
};

use stdweb::web::html_element::CanvasElement;

// Shamelessly stolen from webplatform's TodoMVC example.
macro_rules! enclose {
    ( ($( $x:ident ),*) $y:expr ) => {
        {
            $(let $x = $x.clone();)*
            $y
        }
    };
}


struct State {
    state_enum: StateEnum,
    last_time: f64,
    time_since_beat: f64,
    steps_since_beat: u32,
    ws: WebSocket,
}

impl State {
    pub fn new(ws: WebSocket) -> State {
        State {
            ws: ws,
            state_enum: StateEnum::Waiting,
            last_time: 0.0,
            time_since_beat: 0.0,
            steps_since_beat: 0,
        }
    }
}


enum StateEnum {
    Running(Runner),
    Waiting,
}

fn animate(time: f64, rc: Rc<RefCell<State>>) {
    let t = rc.clone();
    let mut state = t.borrow_mut();

    let mut steps_since_beat = state.steps_since_beat;
    match state.state_enum {
        StateEnum::Running(ref mut runner) => {
            let mut rng = thread_rng();
            runner.run(&mut rng, 1000);
            steps_since_beat += 1000;

            let canvas: CanvasElement = document().query_selector( "#canvas" ).unwrap().unwrap().try_into().unwrap();
            canvas.set_height(runner.histogram.hist_height);
            canvas.set_width(runner.histogram.hist_width);
            let context: CanvasRenderingContext2d = canvas.get_context().unwrap();
            let img_data = context.create_image_data(runner.histogram.hist_width as f64, runner.histogram.hist_height as f64).unwrap();

            let pixels: Vec<u8> = runner.histogram.get_rgba_vec();

            js! (
                @{&img_data}.data.set(@{pixels});
            );

            context.put_image_data(img_data, 0.0, 0.0).unwrap();
        },
        StateEnum::Waiting => {

        },
    };

    let scene_id: u64 = match state.state_enum {
        StateEnum::Running(ref runner) => {
            runner.scene_id
        },
        _ => 0,
    };

    match state.state_enum {
        StateEnum::Running(_) => {
            state.steps_since_beat += 1000;
            if state.time_since_beat > 5000.0 && steps_since_beat > 1000 {
                state.ws.send_bytes(&serialize(&ToServerMsg::Beat(scene_id, state.steps_since_beat)).unwrap()).unwrap();

                state.time_since_beat = 0.0;
                state.steps_since_beat = 0;
            }
        }
        _ => (),
    }


    let dt = time - state.last_time;
    state.time_since_beat += dt;
    state.last_time = time;

    window().request_animation_frame(move |time| {
        animate(time, rc.clone());
    });

}


// fn handle_msg(msg: ToClientMsg) {
//     match msg {
//         ToClientMsg::NewScene(scene) => {
//             new_scene(scene);
//         }
//     }
// }

fn main() {
    stdweb::initialize();

    let output_div: HtmlElement = document().query_selector( ".output" ).unwrap().unwrap().try_into().unwrap();

    let output_msg = Rc::new(move |msg: &str| {
        let elem = document().create_element("p").unwrap();
        elem.set_text_content(msg);
        if let Some(child) = output_div.first_child() {
            output_div.insert_before(&elem, &child).unwrap();
        } else {
            output_div.append_child(&elem);
        }
    });


    output_msg("> Connecting...");

    let ws = WebSocket::new("ws://24.204.205.215:3012").unwrap();
    ws.set_binary_type(SocketBinaryType::ArrayBuffer);
    let state = Rc::new(RefCell::new(State::new(ws.clone())));

    ws.add_event_listener( enclose!( (ws, output_msg) move |_: SocketOpenEvent| {
        ws.send_bytes(&serialize(&ToServerMsg::RequestWork).unwrap()).unwrap();
        output_msg("> Opened connection");
    }));

    ws.add_event_listener( enclose!( (output_msg) move |_: SocketErrorEvent| {
        output_msg("> Connection Errored");
    }));

    ws.add_event_listener( enclose!( (output_msg) move |event: SocketCloseEvent| {
        output_msg(&format!("> Connection Closed: {}", event.reason()));
    }));

    let display_div: HtmlElement = document().query_selector( "#display" ).unwrap().unwrap().try_into().unwrap();

    ws.add_event_listener( enclose!( (state, output_msg) move |event: SocketMessageEvent| {
        let data: Vec<u8> =  event.data().into_array_buffer().unwrap().into();
        let msg = deserialize(&data).unwrap();
        match msg {
            ToClientMsg::NewScene(scene_description) => {
                let width = 1280;
                let height = 720;
                console!(log, "got scene");
                output_msg("> Received new scene");
                let mut rng = thread_rng();

                let canvas: CanvasElement = document().query_selector( "#canvas" ).unwrap().unwrap().try_into().unwrap();
                canvas.set_height(height);
                canvas.set_width(width);

                state.borrow_mut().state_enum = StateEnum::Running(Runner::new(&mut rng, scene_description));
            },
            ToClientMsg::RequestPartialScene => {
                console!(log, "got request for partial scene");
                match state.borrow().state_enum {
                    StateEnum::Running(ref runner) => {
                        console!(log, "sending histo data");
                        state.borrow().ws.send_bytes(&serialize(&ToServerMsg::PartialScene(runner.scene_id, runner.get_partial_scene().clone())).unwrap()).unwrap();
                    },
                    _ => {
                        console!(log, "WEIRDNESS");
                    },
                };
            },
            ToClientMsg::Beat(progress, num_clients) => {
                let msg = format!("progress: {:.02}%; clients: {}", progress * 100.0, num_clients);
                display_div.set_text_content(&msg);
            },
        }
    }));


    let text_entry: InputElement = document().query_selector( ".form input" ).unwrap().unwrap().try_into().unwrap();
    text_entry.add_event_listener( enclose!( (text_entry) move |event: KeyPressEvent| {

        if event.key() == "Enter" {
            event.prevent_default();

            output_msg("> enter");
            let text: String = text_entry.raw_value();
            if text.is_empty() == false {

                text_entry.set_raw_value("");
                // ws.send_text(&text).unwrap();
            }
        }
    }));

    // canvas.set_width(canvas.offset_width() as u32);
    // canvas.set_height(canvas.offset_height() as u32);

    // window().add_event_listener( enclose!( (canvas) move |_: ResizeEvent| {
    //     canvas.set_width(canvas.offset_width() as u32);
    //     canvas.set_height(canvas.offset_height() as u32);
    // }));

    // canvas.add_event_listener( enclose!( (context) move |event: MouseMoveEvent| {
    //     context.fill_rect(f64::from(event.client_x() - 5), f64::from(event.client_y() - 5), 10.0, 10.0);
    // }));

    animate(0., state.clone());

    stdweb::event_loop();
}
